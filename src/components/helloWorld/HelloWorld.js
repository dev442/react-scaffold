import React from 'react';
import config from 'visual-config-exposer';

const HelloWorld = () => {
  return <h1>{config.settings.greeting}</h1>;
};

export default HelloWorld;
