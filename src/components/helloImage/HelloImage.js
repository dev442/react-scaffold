import React from 'react';
import config from 'visual-config-exposer';

const HelloImage = () => {
  return (
    <div>
      <img src={config.settings.greetingImage} />
    </div>
  );
};

export default HelloImage;
