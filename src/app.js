import React from 'react';
import HelloWorld from './components/helloWorld/HelloWorld';
import HelloImage from './components/helloImage/HelloImage';

const App = () => {
  return (
    <div>
      <HelloImage />
      <HelloWorld />
    </div>
  );
};

export default App;
